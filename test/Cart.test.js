const assert = require("assert");
const pricingRules = require("../modules/PricingRules");
const ShoppingCart = require("../modules/Cart");

describe("Testing Cart.js functionalities", function() {
  

  it("should return total price", function() {
    let cart = new ShoppingCart(pricingRules);
    let shouldTotalTo = pricingRules[0].price * 2

    cart.add(pricingRules[0]);
    cart.add(pricingRules[0]);
   
    assert.equal(shouldTotalTo,cart.total);
   
  });



  it("should return all items", function() {
    let cart = new ShoppingCart(pricingRules);
    let shouldTotalTo = [pricingRules[0]]
    cart.add(pricingRules[0]);
    assert.deepEqual(shouldTotalTo,cart.items);
   
  });

  it("should return the right total value of scenario 1",function(){
    let cart = new ShoppingCart(pricingRules);
    let shouldTotalTo = 94.70
    cart.add(pricingRules[0])
    cart.add(pricingRules[0])
    cart.add(pricingRules[0])
    cart.add(pricingRules[2])
    assert.equal(shouldTotalTo,cart.total);
  })

  it("should return the right total value of scenario 2",function(){
    let cart = new ShoppingCart(pricingRules);
    let shouldTotalTo = 209.40
    cart.add(pricingRules[0])
    cart.add(pricingRules[0])
    cart.add(pricingRules[2])
    cart.add(pricingRules[2])
    cart.add(pricingRules[2])
    cart.add(pricingRules[2])
    assert.equal(shouldTotalTo,cart.total);
  })


  it("should return the right total value of scenario 3",function(){
    let cart = new ShoppingCart(pricingRules);
    let shouldTotalTo = 84.70
    cart.add(pricingRules[0])
    cart.add(pricingRules[1])
    cart.add(pricingRules[1])
    assert.equal(shouldTotalTo,cart.total);
  })

  it("should return the right total value of scenario 4",function(){
    let cart = new ShoppingCart(pricingRules);
    let shouldTotalTo = 31.32
    cart.add(pricingRules[0])
    cart.add(pricingRules[3],'I<3AMAYSIM')
    assert.equal(shouldTotalTo,cart.total);
  })


  it("should return the right total items of scenario 3",function(){
    let cart = new ShoppingCart(pricingRules);
    let shouldTotalTo = [
        pricingRules[0],
        pricingRules[1],
        pricingRules[1],
        pricingRules[3],
        pricingRules[3],
    ]
    cart.add(pricingRules[0])
    cart.add(pricingRules[1])
    cart.add(pricingRules[1])
    assert.deepEqual(shouldTotalTo,cart.items);
  })



});
