
// import modules
const pricingRules = require('./modules/PricingRules')
const ShoppingCart = require('./modules/Cart')

// initialize Shopping Cart and pass pricing rules
let cart = new ShoppingCart(pricingRules)

//legend
//pricingRules[0] = ult_small
//pricingRules[1] = ult_medium
//pricingRules[2] = ult_large
//pricingRules[3] = 1gb

// add items
cart.add(pricingRules[0])
cart.add(pricingRules[1])
cart.add(pricingRules[1])


// Output cart total and items
console.log(`CART TOTAL : $${cart.total}`)
console.log(`CART ITEMS : ${cart.items.map(item=>item.productName)}`)

