/**
 * Name : PricingRules
 * Description : This array will hold the pricing rule.
 *   This set of data can be pull from API. Seperating Terms and Pricing Rules gives developers/maintainers more flexibility since very minimal 
 *   modification is needed if terms and price for each product needs to be updated. Each product can have multiple terms or no terms at all. Terms may
 *   have parameters that the specific term requires.
 * Type : Array
 * Properties : 
 *   productCode : String
 *   productName : String
 *   price : Number
 *   terms: Array
*/



const PricingRules = [
  {
    productCode: "ult_small",
    productName: "Unlimited 1GB",
    price: 24.9,
    terms: [{ code: "three_for_two" }]
  },
  {
    productCode: "ult_medium",
    productName: "Unlimited 2GB",
    price: 29.9,
    terms: [
      {
        code: "bundle_with",
        params: {
          bundleWithItem: {
            productCode: "1gb",
            productName: "1 GB Data-pack",
            price: 9.9
          }
        }
      }
    ]
  },
  {
    productCode: "ult_large",
    productName: "Unlimited 5GB",
    price: 44.9,
    terms: [
      {
        code: "bulk_discount",
        params: { discountedPrice: 39.9, itemGreaterThan: 3 }
      }
    ]
  },
  {
    productCode: "1gb",
    productName: "1 GB Data-pack",
    price: 9.9
  }
];

module.exports = PricingRules;
