/**
 * Name : Terms
 * Description : This array will execute the rule and apply the necessary term.
 *   Seperating Terms with Pricing Rules will give Terms much flexibily since it can be reuse and modify without depending on the Pricing Rules.
 * Type : Array
 * Properties : 
 *   code : String
 *   description : String
 *   runRule: Function
*/


const Terms = [ 
  {
    code: "three_for_two",
    description: "A 3 for 2 deal. If you buy any 3 sims, you will pay the price of 2 only for the first month.",
    runRule: (items) => {
      let subtract = 0

      // Get unique item codes
      let uniqueItems = items.map(item => item.productCode).filter((value, index, self) => self.indexOf(value) === index)

      uniqueItems.map(itemCode=>{
        // Get same items
         let sameItems = items.filter(item => item.productCode == itemCode)
          // Check if the number of same items match the condition to apply the discount
         if( sameItems.length  > 2){
           //subtract the discount
            subtract += items[0].price
         }
      })

     // Return the result
      return {
       subtract
      };
    }
  },
  {
    code: "bulk_discount",
    discription:"The Sim will have a bulk discount applied; whereby the price will drop for the first month",
    runRule: (items) => {
      let subtract = 0
      // Get unique item codes
      let uniqueItems = items.map(item => item.productCode).filter((value, index, self) => self.indexOf(value) === index)
    
      uniqueItems.map(itemCode=>{
        // Get same items
         let sameItems = items.filter(item => item.productCode == itemCode)
          // Get the term so that we can get the params
          let term = sameItems[0].terms.filter(t=>t.code == 'bulk_discount')[0]
           // Check if the number of same items match the condition to apply the discount
         if( sameItems.length > term.params.itemGreaterThan){
          sameItems.map(()=>{
            //subtract the discount
            subtract += sameItems[0].price - term.params.discountedPrice
          })
         }
      })

      // Return the result
      return {
        subtract
      };
    }
  },
  {
    code: "bundle_with",
    discription:"This will bundle with free item",
    runRule: (items) => {
      let subtract = 0
      let addItems = []

       // Get unique item codes
      let uniqueItems = items.map(item => item.productCode).filter((value, index, self) => self.indexOf(value) === index)

      uniqueItems.map(itemCode=>{
        // Get same items
         let sameItems = items.filter(item => item.productCode == itemCode)
        
         //Loop through the same items
         sameItems.map((item)=>{
          // Get the term so that we can get the params
          let term = item.terms.filter(t => t.code == 'bundle_with')[0]
          // Add bundle item
          addItems.push(term.params.bundleWithItem)
         })
      })

      // Return the result
      return {
        subtract,
        addItems
      };
    }
  }
];

module.exports = Terms;
