/**
 * Name : Cart
 * Description : This array will execute the rule and apply the necessary term.
 *   Seperating Terms with Pricing Rules will give Terms much flexibily since it can be reuse and modify without depending on the Pricing Rules.
 * Type : Class
 * Properties : 
 *   items : Array
 *   total : Number
 *   add: Function
*/

//import terms array
const Terms = require("./Terms");
const PromoCodes = require("./PromoCodes")

class Cart {

  /**
   * Initialize cart and takes pricing rules as parameter
   * @param {Array[Object]} pricingRules 
   */
  constructor(pricingRules) {
    if (typeof pricingRules != "object") {
      throw new Error("Price Definition must be an object");
    }
    this.pricingRules = pricingRules;
    this.chosenItems = [];
    this.items = [];
    this.total = 0;
  }

  /**
   * Takes an item and an optional promoCode and return total price and all items
   * @param {Object} item 
   * @param {String} promoCode 
   * 
   * @returns {Object} the total price and all items in cart
   */

  add(item, promoCode = null) {
    // Push item to chosen item variable
    this.chosenItems.push(item);

    // run _recomputeTotalAndItems
    let result = this._recomputeTotalAndItems(this.chosenItems);

    // update variables
    this.items = result.items
    this.total = result.total

    // check if promocode is applied
    if(promoCode){
      // get the chosen promo
      let chosenPromo = PromoCodes.filter(promo => promo.code == promoCode)
      // Check promo code if exist
      if(chosenPromo.length > 0){
        switch(chosenPromo[0].unit){
          case 'percent':
            this.total = this.total - (this.total * (chosenPromo[0].discount/100))
          break
          case 'price':
          this.total -= chosenPromo[0].discount
          default:
          break;
        }
        
      }else{
        // Throw an error that the promo is invalid
        throw new Error("Promo code is invalid")
      }
    }

    this.total = parseFloat(this.total.toFixed(2))
    // return result
    return {
      items: this.items,
      total : this.total
    }
  }

  /**
   * Helper function; Will Recompute the total price and return items
   * @param {Array[Object]} items 
   * 
   * @returns {Object} the total price and all items in cart
   */

  _recomputeTotalAndItems(items) {
    // set total price to 0
    let total = 0;

    // loop through all items and get the sum of prices
    items.map((item)=>{
      total += item.price
    })

    // run _applyTerms
    return this._applyTerms(items,Terms,total)
  }

  /**
   * Helper function; Will execute term rule, Re-evaluate the total price and items
   * @param {Array[Object]} items 
   * @param {Terms} terms 
   * @param {Number} total 
   * 
   * @returns {Object} the total price and all items in cart
   */

  _applyTerms(items,terms,total){
   
    // Loop through all terns
    terms.map(term => {

      // Get Items that has the specific term
      let itemsHaveTerm = items.filter(item => {
        if (!item.terms) return false
        return item.terms.some(t=>t.code == term.code)
      })


      // run the rule and get the changes
      let changes = term.runRule(itemsHaveTerm)
      
      // Calculate the changed total
      total -= changes.subtract

      // Check if the term includes adding of item
      if(changes.addItems && changes.addItems.length){

        // Concatinate existing items and items that needs to be added
        items = items.concat(changes.addItems)
      }
    })

    // return total price and all items
    return {total,items}

  }
}

module.exports = Cart;
