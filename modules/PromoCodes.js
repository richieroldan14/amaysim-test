/**
 * Name : PromoCodes
 * Description : This array will hold the list of available promocodes.
 *   This set of data can be pulled from API for flexibility.
 * Type : Array
 * Properties :
 *   code : String
 *   discount : Number
 *   unit: percent|price
 */

const PromoCodes = [
  {
    code: "I<3AMAYSIM",
    discount: 10,
    unit: "percent" // percent = will be calculated in percentage, price = will be deducted to item price
  },
//   {
//     code: "test",
//     discount: 20,
//     unit: "price" // percent = will be calculated in percentage, price = will be deducted to item price
//   }
];

module.exports = PromoCodes;
