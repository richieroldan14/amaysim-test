# Amaysim Technical Test v2 (cart)

### Test requirements
See here : [Requirements](resources/requirements.docx)
### Dev requirements
- [NodeJs and NPM](https://nodejs.org) (version 6.16 up)
- [Mocha](https://mochajs.org/)

### Why NodeJS and Mocha?
- NodeJS is a JavaScript runtime that executes js files. Since the test requirements doesn't need to have GUI's, I can run JS files using NodeJS and Install Mocha package using NPM. 
- Mocha is a testing library. This is a good library for Javascript Unit testing

### Setup and Installation
- If you don't have NodeJS, download and install [here](https://nodejs.org)
- Clone the project
    ```sh
    $ git clone https://gitlab.com/richieroldan14/amaysim-test.git
    cd amaysim-test/
    ```
- Install dependencies (Mocha)
    ```sh
    $ npm install
    ```
### Folder Structure
| Path        | Description
| ------------- |:-------------:|
| /modules    | Modules directory 
| /modules/Cart.js   | Cart module
| /modules/PricingRules.js   | Pricing rules data set
| /modules/PromoCodes.js    | Promo codes data set 
| /modules/Terms.js    | Terms list
| /resources    | Misc files  
| scenario1.js | Scenario 1    
| scenario2.js | Scenario 2  
| scenario3.js |Scenario 3     
| scenario4.js |Scenario 4   
| package.json | Node's package.json file 
| Readme.md | Markdown file for git 

    
### Running Scenarios
- Scenarios are in the root folder of the project named **_scenario1.js, scenario2.js, scenario3.js and scenario4.js_**
- To execute scenarios just run:
   ```sh
    $ node scenario1.js
    $ node scenario2.js
    $ node scenario3.js
    $ node scenario4.js
    ```
- You will see the result in console
- Results : 
    - Scenario 1: ![alt text](resources/scenario1.gif)
    - Scenario 2: ![alt text](resources/scenario2.gif)
    - Scenario 3: ![alt text](resources/scenario3.gif)
    - Scenario 4: ![alt text](resources/scenario4.gif)


    
### Running Tests
- I've created a very simple test for the module Cart.js
- To execute the test just run:
   ```sh
    $ npm test
    ```
- Screenshot :![alt text](resources/test.gif)
### Note
- Each file in module folder has their own documentation and is well documented. Please feel free to read the description in the file header in each file.

##### If you have questions and clarification. Please let me know.  Thank you - Richie Roldan



